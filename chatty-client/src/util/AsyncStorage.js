import AsyncStorage from '@react-native-async-storage/async-storage'

export const storeData = async (key, value) => {
  AsyncStorage.setItem(key, value)
}

export const storeJSONData = async (key, jsonValue) => {
  const stringJsonValue = JSON.stringify(jsonValue)
  AsyncStorage.setItem(key, stringJsonValue)
}

export const getData = async (key) => {
  return AsyncStorage.getItem(key)
}

export const getJSONData = async (key) => {
  const value = await AsyncStorage.getItem(key)
  return value !== null ? JSON.parse(value) : null
}

export const removeItem = async (key) => {
  return AsyncStorage.removeItem(key)
}