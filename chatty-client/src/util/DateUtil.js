
export const dateFormat = (param) => {
    let date = new Date(param).toLocaleString()
    return date.slice(4).slice(0, -8)
}
