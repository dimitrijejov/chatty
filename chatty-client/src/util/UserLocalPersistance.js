import { getJSONData, storeJSONData, removeItem } from './AsyncStorage'

const USER_PERSISTANCE_KEY = 'user'

export const storeUser = async (user) => {
  await storeJSONData(USER_PERSISTANCE_KEY, user)
}

export const getUser = async () => {
  return getJSONData(USER_PERSISTANCE_KEY)
}

export const clearUser = async () => {
  return removeItem(USER_PERSISTANCE_KEY)
}
