import React, { useEffect } from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import HomeScreen from '../screens/HomeScreen'
import NewRoomScreen from '../screens/NewRoomScreen'

import Mqtt from "../mqtt/Mqtt";

const TopTab = createMaterialTopTabNavigator()

export default TabNavigator = () => {
    useEffect(() => {
        Mqtt.getInstance().connect()
    }, []) 

    return (
        <TopTab.Navigator
            tabBarOptions={{
                style: {
                    backgroundColor: 'indigo',
                    // height: 30
                },
               activeTintColor: 'white',
               inactiveTintColor: 'white',
               indicatorStyle: {
                   backgroundColor: 'white',
                   height: 2
               },
               labelStyle:{
                   fontWeight: 'bold'
               }
            }}

            initialRouteName='Home'>
            <TopTab.Screen name="Home" component={HomeScreen} />
            <TopTab.Screen name='New Room' component={NewRoomScreen} />

        </TopTab.Navigator>
    )
}