import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { dateFormat } from '../util/DateUtil'

export default Message = ({ message, sent, sender }) => {
    const [showTime, setShowTime] = useState(false)
    return <View style={styles.showTime}>
        {showTime
            ? <Text>{dateFormat(message.createdAt)}</Text>
            : null}
        <View style={[styles.message, sent ? styles.sent : styles.received]}>
            <TouchableOpacity onPress={() => setShowTime(!showTime)}>
                <Text style={sent ? styles.sentText : styles.receivedText}>{sender.username}</Text>
                <Text style={sent ? styles.sentText : styles.receivedText}>{message.content}</Text>
            </TouchableOpacity>
        </View>
    </View>
}

const styles = StyleSheet.create({
    showTime:{
        alignItems: 'center'
    },
    sent: {
        alignSelf: 'flex-end',
        backgroundColor: '#6b22a2',
    },
    received: {
        alignSelf: "flex-start",
        backgroundColor: '#d3d3d3'
    },
    sentText: {
        color: 'white',
    },
    receivedText: {
        color: 'black',
    },
    message: {
        margin: 3,
        maxWidth: '80%',
        borderRadius: 15,
        padding: 10
    }
})
