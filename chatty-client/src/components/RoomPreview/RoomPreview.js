import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Touchable } from 'react-native'
import RoomPreviewModelView from './RoomPreviewModelView'
import { ListItem } from 'react-native-elements'

export default RoomPreview = ({ room, roomPress, navigation }) => {
  const [data, setData] = useState(null)

  useEffect(() => {
    new RoomPreviewModelView().init(room)
      .then(setData)
  }, [])

  useEffect(() => {
    const subscribe = navigation.addListener('focus', async () => {
      new RoomPreviewModelView().init(room)
      .then(setData)
    });

    return subscribe;
  }, [navigation]);

  if (!data) {
    return <Text>Loading...</Text>
  }

  const { message, sender } = data.last

  return (
    <ListItem bottomDivider onPress={() => roomPress(data.room.id, data.room.name)}>
      <ListItem.Content>
        <ListItem.Title>{data.room.name}</ListItem.Title>
        {message
        ? (
          <ListItem.Subtitle>{sender.username}: {message.content}</ListItem.Subtitle>
        )
        : (
          <ListItem.Subtitle>Start a conversation...</ListItem.Subtitle>
        )}
      </ListItem.Content>
    </ListItem>
  )
}

const styles = StyleSheet.create({})

