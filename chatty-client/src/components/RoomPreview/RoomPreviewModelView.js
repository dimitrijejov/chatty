import { getRoomLastMessage } from '../../service/RoomService'
import { getUser } from '../../service/UserService'

export default class RoomPreviewModelView {
  async init(room) {
    let last = {}
    last.message = await getRoomLastMessage(room.id)
    if (last.message) {
      last.sender = await getUser(last.message.senderId)
    }

    return {
      room,
      last
    }
  }
}
