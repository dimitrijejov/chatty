import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Touchable } from 'react-native'
import { ListItem } from 'react-native-elements'
import { Feather } from '@expo/vector-icons';

export default RoomPreview = ({ user, userPress, navigation }) => {
    const [selected, setSelected] = useState(false)

    const listItemPress = (id) => {
        setSelected(!selected)
        userPress(id)
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
          setSelected(false)
        });
    
        return unsubscribe;
      }, [navigation]);

    return (
        <ListItem bottomDivider onPress={() => listItemPress(user.id)}>
            <ListItem.Content>
                <ListItem.Title>{user.username}</ListItem.Title>
                <ListItem.Subtitle>{user.firstName} {user.lastName}</ListItem.Subtitle>
            </ListItem.Content>
            {selected ?
                <View>
                    <Feather name="check" size={24} color="black" />
                </View>
                : null
            }
        </ListItem>
    )
}

const styles = StyleSheet.create({
    container: {

    }
})