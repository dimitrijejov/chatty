import config from '../../config/config'
import axios from "axios";
import { storeUser } from '../util/UserLocalPersistance'


export const getUser = (userId) => {
  return axios.get(`${config.server.url}/users/${userId}`)
    .then(res => res.data)
}

export const register = async (user) => {
  const response = await axios.post(`${config.server.url}/register`, user, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
  await storeUser(response.data)
  return response
}

export const getAllUsers = () => {
  return axios.get(`${config.server.url}/users/all`)
    .then(res => res.data)
}
