import config from '../../config/config'
import axios from "axios";

export const getUserRooms = (userId) => {
  return axios.get(`${config.server.url}/users/${userId}/rooms`)
    .then(res => res.data)
}

export const getRoomUsers = (roomId) => {
  return axios.get(`${config.server.url}/rooms/${roomId}/users`)
    .then(res => res.data)
}

export const getRoom = (roomId) => {
  return axios.get(`${config.server.url}/rooms/${roomId}`)
    .then(res => res.data)
}

export const getRoomLastMessage = (roomId) => {
  return axios.get(`${config.server.url}/rooms/${roomId}/last-message`)
    .then(res => res.data)
}

export const getRoomMessages = (roomId) => {
  return axios.get(`${config.server.url}/rooms/${roomId}/messages`)
    .then(res => res.data)
}

export const createNewRoom = (name, roomUsers) => {
  return axios.post(`${config.server.url}/rooms`,
    {
      name, roomUsers
    }, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(res => res.data)
}