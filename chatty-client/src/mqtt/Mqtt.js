import config from '../../config/config'
import init from 'react_native_mqtt'
import AsyncStorage from '@react-native-async-storage/async-storage'

let i = 0

export default class MqttClient {
  static instance = null

  /**
   * @returns {MqttClient}
   */
  static getInstance () {
    if (!this.instance) {
      this.instance = new MqttClient()
    }

    return this.instance
  }

  constructor() {
    this.topicCallbackMap = new Map()
  }

  connect () {
    init({
      size: 10000,
      storageBackend: AsyncStorage,
      defaultExpires: 1000 * 3600 * 24,
      enableCache: false,
      reconnect: true,
      sync : {}
    });

    this.client = new Paho.MQTT.Client(config.mqtt.brokerUrl, config.mqtt.port, config.mqtt.clientId)
    this.client.onMessageArrived = (msg) => this._onMessage(msg)

    this.client.connect({
      onSuccess: () => console.log(`Connected on broker ${config.mqtt.brokerUrl}:${config.mqtt.port} successfully`),
      onFailure: () => console.log(`Failed to connect on broker ${config.mqtt.brokerUrl}:${config.mqtt.port}`),
      useSSL: false
    }) 
  }

  _onMessage (message) {
    const topicCallback = this.topicCallbackMap.get(message.topic)

    if (topicCallback) {
      topicCallback(message.payloadString)
    }
  }

  subscribe (topic, callback) {
    this.client.subscribe(topic, {
      onSuccess: () => {
        console.log(`Successful subscription on topic ${topic}`)
        this.topicCallbackMap.set(topic, callback)
      },
      onFailure: () => console.log(`Subscription failed on topic ${topic}`)
    })
  }

  unsubscribe (topic) {
    this.topicCallbackMap.delete(topic)
    this.client.unsubscribe(topic, { onSuccess: () => console.log(`Successful unsubscribe from ${topic}`)})
  }

  publish (topic, message) {
    this.client.publish(topic, message)
  }

  isConnected () {
    return this.client.isConnected()
  }
  
}
