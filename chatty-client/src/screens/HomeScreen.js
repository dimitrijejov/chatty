import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Button, FlatList, ScrollView, RefreshControl, Image } from 'react-native';
import RoomPreview from '../components/RoomPreview/RoomPreview';
import { getUser } from '../util/UserLocalPersistance'

import { getUserRooms } from "../service/RoomService";


export default HomeScreen = ({ navigation }) => {
  const [user, setUser] = useState({})
  const [rooms, setRooms] = useState([])
  const [refreshing, setRefreshing] = useState(false);
  
  const onRefresh = () => {
    setRefreshing(true);
    getUserRooms(user.id).then(setRooms).then(setRefreshing(false))
  }

  useEffect(() => {
    const subscribe = navigation.addListener('focus', () => {
      getUserRooms(user.id).then(setRooms)
    });

    return subscribe;
  }, [navigation]);


  useEffect(() => {
    getUser()
      .then(user => {
        setUser(user)
        getUserRooms(user.id).then(setRooms)
      })
  }, [])

  const onPressRoomPreview = (id, name) => {
    navigation.navigate('Room', { id, name });
  }

  return (
    <ScrollView style={styles.container}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <StatusBar style="auto" />
      <View>
        <FlatList
          nestedScrollEnabled
          data={rooms}
          renderItem={({ item, index }) => {
            return <RoomPreview room={item} roomPress={onPressRoomPreview} navigation={navigation} />
          }}
          keyExtractor={el => el.id.toString()} />
      </View>
      <Image
        style={{ width: 100, height: 50 }}
        source={require('../../assets/chatty.png')}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    ...StyleSheet.absoluteFillObject
  },
});
