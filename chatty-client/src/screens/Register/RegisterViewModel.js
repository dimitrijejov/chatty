
class RegisterViewModel {
  constructor() {
    this.phoneNumber = ''
    this.username = ''
    this.firstName = ''
    this.lastName =''
  }

  toJSON() {
    return {
      phoneNumber: this.getPhoneNumber(),
      username: this.getUsername(),
      firstName: this.getFirstName(),
      lastName: this.getLastName()
    }
  }

  reset() {
    this.setPhoneNumber('')
    this.setUsername('')
    this.setFirstName('')
    this.setLastName('')
  }
  
  setPhoneNumber (phoneNum) {
    this.phoneNumber = phoneNum
  }

  setUsername (username) {
    this.username = username
  }

  setFirstName (firstName) {
    this.firstName = firstName
  }

  setLastName (lastName) {
    this.lastName = lastName
  }

  getPhoneNumber () {
    return this.phoneNumber
  }

  getUsername () {
    return this.username
  }

  getFirstName () {
    return this.firstName
  }

  getLastName () {
    return this.lastName
  }
}

let registerModelViewInstance = null

/**
 * @returns {RegisterViewModel}
 */
export function getRegisterViewModel ()  {
  if (!registerModelViewInstance) {
    registerModelViewInstance = new RegisterViewModel()
  }

  return registerModelViewInstance
}
