import React, { useEffect, useState } from 'react'
import { View, ActivityIndicator, Text } from 'react-native'
import { Button } from 'react-native-elements'
import style from './RegisterStyles'

import { getRegisterViewModel } from './RegisterViewModel'
import { register } from '../../service/UserService'

const Content = ({ error, tryAgain }) => {
  if (error) {
    return (
      <>
        <Text style={style.errorMessage}>Something went wrong</Text>
        <Button title='Try Again' onPress={tryAgain} />
      </>
    )
  } else {
    return <ActivityIndicator size='large' />
  }
}

const RegisterLoaderScreen = ({ navigation }) => {
  const [error, setError] = useState(false)
  
  useEffect(() => {
      registerAction()
  }, [])

  const registerAction = () => {
    register(getRegisterViewModel().toJSON())
      .then(res => {
        getRegisterViewModel().reset()
        navigation.reset({ index: 0, routes: [{ name: 'Root' }]})
      })
      .catch(err => setError(true))
  } 

  const onTryAgain = () => {
    setError(false)
    registerAction()
  }

  return (
    <View style={style.screenContainer}>
      <Content error={error} tryAgain={onTryAgain} />
    </View>
  )
}

export default RegisterLoaderScreen