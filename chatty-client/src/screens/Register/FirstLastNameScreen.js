import React, { useState } from 'react'
import { View, StyleSheet } from 'react-native'
import { Input, Button, Text } from 'react-native-elements'
import style from './RegisterStyles'
import { getRegisterViewModel } from './RegisterViewModel'

const isEmptyString = string => !string || string === ''

const UsernameScreen = ({ navigation, route }) => {
  const [firstName, setFirstName] = useState(getRegisterViewModel().getFirstName())
  const [lastName, setLastName] = useState(getRegisterViewModel().getLastName())
  const [firstNameError, setFirstNameError] = useState(false)
  const [lastNameError, setLastNameError] = useState(false)

  const onPress = () => {
    if (isEmptyString(firstName) && !isEmptyString(lastName)) {
      setFirstNameError(true)
    } else if (!isEmptyString(firstName) && isEmptyString(lastName)) {
      setLastNameError(true)
    } else {
      getRegisterViewModel().setFirstName(firstName)
      getRegisterViewModel().setLastName(lastName)
      navigation.navigate('RegisterLoaderScreen')
    }
  }

  const onChangeTextFirstName = (value) => {
    setFirstName(value)
    setFirstNameError(false)
  }

  const onChangeTextLastName = (value) => {
    setLastName(value)
    setLastNameError(false)
  }

  const buttonName = (isEmptyString(firstName) && isEmptyString(lastName)) ? 'Skip' : 'Submit'
  const firstNameErrorMessage = firstNameError ? 'Must enter first name if last name is provided' : ''
  const lastNameErrorMessage = lastNameError ? 'Must enter last name if first name is provided' : ''

  return (
    <View style={style.screenContainer}>
      <Input
        errorMessage={firstNameErrorMessage}
        placeholder='John'
        onChangeText={onChangeTextFirstName}
        value={firstName}
        label='First Name'
      />
      <Input
        errorMessage={lastNameErrorMessage}
        placeholder='Doe'
        onChangeText={onChangeTextLastName}
        value={lastName}
        label='Last Name'
      />
      <View style={style.buttonContainer}>
        <Button style={style.nextButton} title={buttonName} onPress={onPress} />
      </View>
    </View>
  )
}

export default UsernameScreen