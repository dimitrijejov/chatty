import React, { useState, useEffect } from 'react'
import { View, StyleSheet } from 'react-native'
import { Input, Button } from 'react-native-elements'
import { getRegisterViewModel } from './RegisterViewModel'

const style = StyleSheet.create({
  screenContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: 16
  },
  buttonContainer: {
    paddingVertical: 4,
    paddingHorizontal: 16
  },
  nextButton: {
    borderRadius: 16
  }
})

const PhoneNumberScreen = ({ navigation }) => {
  const [phoneNumber, setPhoneNumber] = useState(getRegisterViewModel().getPhoneNumber())
  const [error, setError] = useState(false)

  const next = () => {
    if (phoneNumber.length === 0) {
      setError(true)
    } else {
      getRegisterViewModel().setPhoneNumber(phoneNumber)
      navigation.navigate('UsernameScreen')
    }
  }

  const onChangeText = (value) => {
    setPhoneNumber(value)
    setError(false)
  }

  const errorMessage = error ? 'Phone number required' : ''

  return (
    <View style={style.screenContainer}>
      <Input
        errorMessage={errorMessage}
        placeholder='+381 62 ...'
        onChangeText={onChangeText}
        keyboardType='phone-pad'
        value={phoneNumber}
        label='Phone Number'
      />
      <View style={style.buttonContainer}>
        <Button style={style.nextButton} title="Next" onPress={next} />
      </View>
    </View>
  )
}

export default PhoneNumberScreen
