import React, { useState } from 'react'
import { View } from 'react-native'
import { Input, Button } from 'react-native-elements'
import { getRegisterViewModel } from './RegisterViewModel'
import style from './RegisterStyles'

const UsernameScreen = ({ navigation, route }) => {
  const [username, setUsername] = useState(getRegisterViewModel().getUsername())
  const [error, setError] = useState(false)
  const [noSpaceError, setNoSpaceError] = useState(false)

  const next = () => {
    if (username.length === 0) {
      setError(true)
    } else {
      getRegisterViewModel().setUsername(username)
      navigation.navigate('FirstLastNameScreen')
    }
  }

  const onChangeText = (value) => {
    if (value.length > 0 && value[value.length - 1] === ' ') {
      setNoSpaceError(true)
    }

    setUsername(
      value.replace(/\s/g, '') // should remove white spaces
    )
    setError(false)

    noSpaceError && setNoSpaceError(false)
  }

  const errorMessage = error ? 'Username required' : ''
  const noSpaceErrorMessage = noSpaceError ? 'Spaces not allowed' : ''
  
  return (
    <View style={style.screenContainer}>
      <Input
        errorMessage={errorMessage || noSpaceErrorMessage}
        placeholder='superuser'
        onChangeText={onChangeText}
        autoCapitalize='none'
        value={username}
        label='Username'
      />
      <View style={style.buttonContainer}>
        <Button style={style.nextButton} title="Next" onPress={next} />
      </View>
    </View>
  )
}

export default UsernameScreen