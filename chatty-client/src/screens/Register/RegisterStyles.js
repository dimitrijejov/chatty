import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  screenContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: 16
  },
  buttonContainer: {
    paddingVertical: 4,
    paddingHorizontal: 16
  },
  nextButton: {
    borderRadius: 16
  },
  errorMessage: {
    color: '#ed4337'
  },
  screenLoaderContainer : {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16
  }
})