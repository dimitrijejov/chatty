import React, { useEffect, useState, useRef } from 'react';
import { StyleSheet, View, Button, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Ionicons } from '@expo/vector-icons'; 
import Message from '../../components/Message';
import { Text } from 'react-native-elements';

import { getUser } from '../../util/UserLocalPersistance'
import { getRoomViewModel } from './RoomViewModel'
import MqttClient from '../../mqtt/Mqtt'

export default ChatScreen = ({ route, navigation }) => {
  const [room, setRoom] = useState({})
  const [messages, setMessages] = useState([])
  const [users, setUsers] = useState(new Map())
  const [newMessage, setNewMessage] = useState('')
  const [showSend, setShowSend] = useState(false)
  const [user, setUser] = useState(getUser())
  const scrollViewRef = useRef();


  useEffect(() => {
    const subscribe = navigation.addListener('blur', () => {
      navigation.navigate('Home');
    });

    return subscribe;
  }, [navigation]);
  
  useEffect(() => {
    const roomId = route.params.id

    navigation.setOptions({title: route.params.name})
    if (roomId) {
      getRoomViewModel().getData(roomId)
        .then(data => {
          setRoom(data.room)
          setUsers(data.users)
          setMessages(data.messages)
          setUser(data.user)
        })
    }

    MqttClient.getInstance()
      .subscribe(`messages/${roomId}`, (msg) => onMessage(msg))

    return () => {
      getRoomViewModel().clear()
      MqttClient.getInstance()
        .unsubscribe(`messages/${roomId}`)
    }
  }, [])

  const onMessage = (msg) => {
    const payload = JSON.parse(msg)
    getRoomViewModel().setMessages(
      [...getRoomViewModel().getMessages(), payload.data]
    )

    setMessages(getRoomViewModel().getMessages())
  }

  const onSend = () => {
    if (newMessage !== '') {
      const payload = JSON.stringify({ params: _getMessage(newMessage)})
      MqttClient.getInstance().publish('messages/save', payload)
      setNewMessage('')
    }
  }

  const _getMessage = (msgContent) => {
    return {
      roomId: room.id,
      senderId: user.id,
      content: msgContent,
      isMedia: false
    }
  }

  const showMessages = () => {
    let res = []

    messages.map((item, index) => (
      res.push(
        <Message message={item} sent={user.id === item.senderId} sender={users.get(item.senderId)} key={index.toString()} />
      )
    ))

    return res
  }
  return (
    <View style={styles.container}>
      {room.id
        ? (
          <View style={styles.room}>
            <View style={styles.messageList}>
              <ScrollView
                contentContainerStyle={styles.flatList}
                ref={scrollViewRef}
                onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}>
                {showMessages()}
              </ScrollView>
            </View>
            <View style={styles.newMessage}>
              <TextInput
                style={styles.input}
                multiline
                autoCorrect={false}
                value={newMessage}
                placeholder="Type here..."
                onChangeText={newValue => setNewMessage(newValue)}
                onFocus={() => setShowSend(true)}
                onBlur={() => setShowSend(false)}
              />
              {showSend
                ? <TouchableOpacity
                  title='Send'
                  style={styles.sendBtn}
                  onPress={onSend}
                >
                  <Ionicons name="send" size={24} color="black" />
                </TouchableOpacity>
                : null}
            </View>
          </View>
        )
        : (
          <Text>Loading...</Text>
        )
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    ...StyleSheet.absoluteFillObject
  },
  room: {
    ...StyleSheet.absoluteFillObject
  },
  messageList: {
    flex: 9
  },
  flatList: {
    flexGrow: 1,
    justifyContent: 'flex-end'
  },
  newMessage: {
    flexDirection: 'row'
  },
  input: {
    padding: 5,
    paddingLeft: 10,
    margin: 10,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 20,
    flex: 9
  },
  sendBtn: {
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 50,
    padding: 5,
    paddingLeft: 10,
    marginVertical: 10,
    marginRight: 10,
    flex: 1
  }

});