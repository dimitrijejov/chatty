import { getRoom, getRoomMessages, getRoomUsers } from '../../service/RoomService'
import { getUser as getLoggedInUser } from '../../util/UserLocalPersistance'

class RoomViewModel {
  constructor () {
    // maybe we don't need this
    this.user = null
    this.users = new Map()
    this.messages = []
    this.room = null
  }

  async getData (roomId) {
    const room = getRoom(roomId)
    const roomMessages = getRoomMessages(roomId)
    const roomUsers = getRoomUsers(roomId)
    const loggedInUser = getLoggedInUser()

    this.room = await room
    this.messages = await roomMessages
    this.user = await loggedInUser
    const users = await roomUsers
    users.forEach(user => this.users.set(user.id, user))

    return {
      room: this.room,
      user: this.user,
      users: this.users,
      messages: this.messages

    }
  }

  setMessages (msgs) {
    this.messages = msgs
  }

  getMessages () {
    return this.messages
  }

  clear() {
    this.user = null
    this.users = new Map()
    this.messages = []
    this.room = null
  }
}

let roomViewModelInstance = null

/**
 * @returns {RoomViewModel}
 */
export function getRoomViewModel () {
  if (!roomViewModelInstance) {
    roomViewModelInstance = new RoomViewModel()
  }

  return roomViewModelInstance
}