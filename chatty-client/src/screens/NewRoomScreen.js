import React, { useEffect, useState } from 'react';
import UserPreview from '../components/UserPreview';
import { StyleSheet, View, Button, FlatList, TouchableOpacity, TextInput, ScrollView, StatusBar } from 'react-native';
import { Text, Input, Icon } from 'react-native-elements';
import { getUser } from '../util/UserLocalPersistance'

import { getAllUsers } from "../service/UserService";
import { createNewRoom } from '../service/RoomService'

export default NewRoomScreen = ({ navigation }) => {
  const [user, setUser] = useState({})
  const [users, setUsers] = useState([])
  const [roomName, setRoomName] = useState("")
  const [roomUsers, setRoomUsers] = useState([])
  const [error, setError] = useState({
    true: false,
    text: ''
  })

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      setRoomName("")
      setRoomUsers([])
      setError({true: false, text: ''})
    });

    // return unsubscribe
  }, [navigation]);

  useEffect(() => {
    getUser()
      .then(user => {
        setUser(user)
        getAllUsers().then(setUsers)
      })
  }, [])

  const createRoom = async () => {
    if (roomUsers.length === 0) {
      setError({ true: true, text: 'Select users for the room!' })
    } else if (roomName === "") {
      setError({true: true, text: 'No room name set!'})
    } else {
      const loggedInUser = await getUser()
      setError({true: false, text: ''})
      let res = await createNewRoom(roomName, [...roomUsers, loggedInUser.id])
      navigation.navigate('Room', { id: res.id, name: res.name });
    }
  }


  const onPressUserPreview = (id) => {
    let arr = [...roomUsers]
    if (!arr.includes(id)) {
      arr.push(id);
    } else {
      arr.splice(arr.indexOf(id), 1);
    }
    setRoomUsers(arr)
  }

  return (
    <ScrollView style={styles.container}>
      <Text h4>Give your room a name:</Text>
      <StatusBar  barStyle="light-content" translucent={true} />
      <View style={styles.newRoomText}>
        <TextInput
          style={styles.input}
          autoCapitalize="none"
          autoCorrect={false}
          value={roomName}
          placeholder="Type here..."
          onChangeText={newValue => setRoomName(newValue)}
        />
        <TouchableOpacity
          title='Send'
          style={styles.createBtn}
          onPress={() => createRoom()}>
          <Text>Create</Text>
        </TouchableOpacity>
      </View>
      {error.true
          ? <Text style={{ color: 'red', marginHorizontal: 10 }}>{error.text}</Text>
          : null}
      <Text h4>Select members:</Text>
      <View>
        <FlatList
          data={users}
          renderItem={({ item, index }) => {
            if (item.id !== user.id) {
              return <UserPreview user={item} userPress={onPressUserPreview} navigation={navigation} />
            }
          }}
          keyExtractor={el => el.id.toString()} />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    ...StyleSheet.absoluteFillObject
  },
  newRoomText: {
    flexDirection: 'row'
  },
  input: {
    padding: 5,
    paddingLeft: 10,
    margin: 10,
    borderColor: 'black',
    borderWidth: 1,
    flex: 8
  },
  createBtn: {
    borderColor: 'black',
    borderWidth: 1,
    padding: 5,
    paddingTop: 9,
    marginVertical: 10,
    marginRight: 10,
    flex: 2,
    alignItems: 'center'
  }
});