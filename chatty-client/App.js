import React, { useEffect, useState } from 'react'
import { Image } from 'react-native'

import Mqtt from './src/mqtt/Mqtt'
console.disableYellowBox = true;
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import TabNavigation from './src/navigation/TabNavigation'

import RoomScreen from './src/screens/Room/RoomScreen'

// Register
import PhoneNumber from './src/screens/Register/PhoneNumberScreen'
import UsernameScreen from './src/screens/Register/UsernameScreen'
import FirstLastNameScreen from './src/screens/Register/FirstLastNameScreen'
import RegisterLoaderScreen from './src/screens/Register/RegisterLoaderScreen'

import { getUser, storeUser, clearUser } from './src/util/UserLocalPersistance'

const Stack = createStackNavigator()

const App = () => {
  const [userLoggedIn, setUserLoggedIn] = useState(false)
  const [checkingIfLoggedIn, setCheckingIfLoggedIn] = useState(true)

  useEffect(() => {
    storeUser({
      id: 9,
      phoneNumber: '+38162275602',
      username: 's.covic_',
      firstName: 'Stefan',
      lastName: 'Covic'
    })
    .then(() => getUser())
    .then(user => {
      setUserLoggedIn(user !== null)
      setCheckingIfLoggedIn(false)
    })

    // clearUser()
    //   .then(() => getUser())
    //   .then(user => {
    //     setUserLoggedIn(user !== null)
    //     setCheckingIfLoggedIn(false)
      // })

    // getUser()
    //   .then(user => {
    //     setUserLoggedIn(user !== null)
    //     setCheckingIfLoggedIn(false)
    //   })
  }, [])

  if (checkingIfLoggedIn) {
    return null
  }

  const initialRouteName = userLoggedIn ? 'Root' : 'PhoneNumberScreen'

  const LogoTitle = () => {
    return (
      <Image
        style={{ marginTop: 20, marginLeft: 10, width: 95, height: 50 }}
        source={require('./assets/chatty.png')}
      />
    );
  }

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{
        headerStyle: {
          backgroundColor: 'indigo',
          shadowOpacity: 0,
          elevation: 0,
          // height: 75
        },
        headerTintColor: 'white'
      }}
        initialRouteName={initialRouteName}
      >
        <Stack.Screen name='Root' component={TabNavigation}
           options={{ headerTitle: props => <LogoTitle {...props} /> }} />

        <Stack.Screen name='Room' component={RoomScreen} />

        <Stack.Screen
          name='PhoneNumberScreen'
          component={PhoneNumber}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name='UsernameScreen'
          component={UsernameScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name='FirstLastNameScreen'
          component={FirstLastNameScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name='RegisterLoaderScreen'
          component={RegisterLoaderScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App
