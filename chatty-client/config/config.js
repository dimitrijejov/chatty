import {
    BACKEND_URL,
    MQTT_BROKER_URL,
    MQTT_BROKER_PORT,
    MQTT_CLIENT_ID
} from '@env'

// BACKEND
const backendURL = BACKEND_URL || 'http://192.168.1.7:4050'

// MQTT
const mqttBrokerUrl = MQTT_BROKER_URL || ''
const mqttPort = MQTT_BROKER_PORT || 1883
const mqttClientId = MQTT_CLIENT_ID || ''


export default {
    server: {
        url: backendURL
    },
    mqtt: {
        brokerUrl: mqttBrokerUrl,
        port: Number(mqttPort),
        clientId: mqttClientId
    }
}