
module.exports = class Message {
  constructor({ id, roomId, senderId, content, isMedia, createdAt, updatedAt }) {
    this.id = id || -1
    this.roomId = roomId
    this.senderId = senderId
    this.content = content
    this.isMedia = isMedia || false
    this.createdAt = createdAt || new Date()
    this.updatedAt = updatedAt || new Date()
  }

  toJSON () {
    return {
      id: this.id,
      roomId: this.roomId,
      senderId: this.senderId,
      content: this.content,
      isMedia: this.isMedia,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }
  }
}
