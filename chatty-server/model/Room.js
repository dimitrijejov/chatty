
module.exports = class Room {
  constructor({ id, name, directMessaging, createdAt, updatedAt }) {
    this.id = id || -1
    this.name = name
    this.directMessaging = directMessaging || false
    this.createdAt = createdAt || new Date()
    this.updatedAt = updatedAt || new Date()
  }
}
