
module.exports = class RoomUserMap {
  constructor ({ id, roomId, userId, createdAt, updatedAt }) {
    this.id = id  || -1
    this.roomId = roomId
    this.userId = userId
    this.createdAt = createdAt  || new Date()
    this.updatedAt = updatedAt || new Date()
  }
}
