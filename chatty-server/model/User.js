
module.exports = class User {
  constructor({ id, phoneNumber, username, firstName, lastName, createdAt, updatedAt }) {
    this.id = id || -1
    this.phoneNumber = phoneNumber
    this.username = username
    this.firstName = firstName || ''
    this.lastName = lastName || ''
    this.createdAt = createdAt || new Date()
    this.updatedAt = updatedAt || new Date()
  }
}
