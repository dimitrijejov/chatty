const mqtt = require('mqtt')
const config = require('../config/config')

class MqttClient {
  constructor() {
    this.client = mqtt.connect(config.mqtt.brokerUrl)
    this.topics = new Map()

    this.client.on('connect', () => {
      console.log(`Connected on broker ${config.mqtt.brokerUrl}`)
    })

    this.client.on('error', (err) => {
      console.error(`Mqtt Err`, err)
    })

    this.client.on('message', (topic, message) => this._onMessage(topic, message))
  }

  _onMessage (topic, message) {
    const messageHandler = this.topics.get(topic)
    const stringMessage = message.toString()

    if (messageHandler) {
      messageHandler.execute(JSON.parse(stringMessage))
    } else {
      console.warn('No callback for topic:', topic)
    }
  }

  subscribe (topic, msgHandler) {
    this.client.subscribe(topic, (err) => {
      if (err) {
        console.error(`Subscription to topic '${topic}' failed`)
      } else {
        console.log(`Subscription to topic '${topic}' success`)
        this.topics.set(topic, msgHandler)
      }
    })
  }

  publish (topic, message) {
    this.client.publish(topic, message)
  }
}

let instance = null

module.exports.getMqttClient = () => {
  if (!instance) {
    instance = new MqttClient()
  }

  return instance
}

module.exports.MqttClient = MqttClient
