const MessageService = require('../service/MessageService')
const Message = require('../model/Message')

module.exports = class SaveMessageMqttHandler {
  /**
   * @param {MessageService} messageService 
   */
  constructor (messageService) {
    this.messageService = messageService
  }

  getTopic () {
    return 'messages/save'
  }

  async execute (message) {
    const { params } = message
    const createdMessage = await this.messageService.createMessage(new Message({
      roomId: params.roomId,
      senderId: params.senderId,
      content: params.content,
      isMedia: params.isMedia
    }))

    this.messageService.publishMessage(`messages/${params.roomId}`, createdMessage)
  }
}
