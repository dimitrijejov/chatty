const ServiceFactory = require('./ServiceFactory')
const { MESSAGE_SERVICE } = require('./ServiceFactory')
const { getMqttClient } = require('../mqtt/MqttClient')
const SaveMessageMqttHandler = require('../mqtt/SaveMessageMqttHandler')

module.exports = class MqttHandlerFactory {
  /**
   * @param {ServiceFactory} serviceFactory 
   */
  constructor(serviceFactory) {
    this.mqttClient = getMqttClient()
    this.serviceFactory = serviceFactory
  }

  setupHandlers () {
    const saveMessageMqttHandler = new SaveMessageMqttHandler(
      this.serviceFactory.getService(MESSAGE_SERVICE)
    )

    this.mqttClient.subscribe(saveMessageMqttHandler.getTopic(), saveMessageMqttHandler)
  }
}
