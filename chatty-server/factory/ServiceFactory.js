const RepositoryFactory = require('./RepositoryFactory')
const { getMqttClient } = require('../mqtt/MqttClient')
const { 
  USER_REPOSITORY,
  MESSAGE_REPOSITORY,
  ROOM_REPOSITORY,
  ROOM_USER_MAP_REPOSITORY 
} = require('./RepositoryFactory')

const UserService = require("../service/UserService")
const RoomService = require("../service/RoomService")
const MessageService = require('../service/MessageService')

const USER_SERVICE = 'USER_SERVICE'
const ROOM_SERVICE = 'ROOM_SERVICE'
const MESSAGE_SERVICE = 'MESSAGE_REPOSITORY'

module.exports = class ServiceFactory {
  /**
   * @param {RepositoryFactory} repositoryFactory 
   */
  constructor(repositoryFactory) {
    this.map = new Map()

    this.map.set(USER_SERVICE, new UserService(
      repositoryFactory.getRepository(USER_REPOSITORY),
      repositoryFactory.getRepository(ROOM_USER_MAP_REPOSITORY)
    ))
    this.map.set(MESSAGE_SERVICE, new MessageService(
      repositoryFactory.getRepository(MESSAGE_REPOSITORY),
      getMqttClient()
    ))

    this.map.set(ROOM_SERVICE, new RoomService(
      repositoryFactory.getRepository(ROOM_REPOSITORY),
      repositoryFactory.getRepository(ROOM_USER_MAP_REPOSITORY),
      repositoryFactory.getRepository(MESSAGE_REPOSITORY)
    ))
  }

  getService(serviceName) {
    switch (serviceName) {
      case USER_SERVICE: {
        return this.map.get(serviceName)
      }
      case MESSAGE_SERVICE: {
        return this.map.get(serviceName)
      }
      case ROOM_SERVICE: {
        return this.map.get(serviceName)
      }
      default:
        throw new Error(`Unknown repository name - ${serviceName}`)
    }
  }
}

module.exports.USER_SERVICE = USER_SERVICE
module.exports.MESSAGE_SERVICE = MESSAGE_SERVICE
module.exports.ROOM_SERVICE = ROOM_SERVICE
