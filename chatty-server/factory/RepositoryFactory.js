// USER
const UserDatasource = require("../data/User/datasource/UserDatasource")
const UserRepository = require("../data/User/repository/UserRepository")

// MESSAGE
const MessageDatasource = require('../data/Message/datasource/MessageDatasource')
const MessageRepository = require('../data/Message/repository/MessageRepository')

// ROOM
const RoomDatasource = require("../data/Room/datasource/RoomDatasource")
const RoomRepository = require("../data/Room/repository/RoomRepository")

// ROOM_USER_MAP
const RoomUserMapDatasource = require('../data/RoomUserMap/datasource/RoomUserMapDatasource')
const RoomUserMapRepository = require('../data/RoomUserMap/repository/RoomUserMapRepository')

const USER_REPOSITORY = 'USER_REPOSITORY'
const ROOM_REPOSITORY = 'ROOM_REPOSITORY'
const MESSAGE_REPOSITORY = 'MESSAGE_REPOSITORY'
const ROOM_USER_MAP_REPOSITORY = 'ROOM_USER_MAP_REPOSITORY'

module.exports = class RepositoryFactory {
  constructor() {
    this.map = new Map()

    this.map.set(USER_REPOSITORY, new UserRepository(new UserDatasource()))
    this.map.set(MESSAGE_REPOSITORY, new MessageRepository(new MessageDatasource()))
    this.map.set(ROOM_REPOSITORY, new RoomRepository(new RoomDatasource()))
    this.map.set(ROOM_USER_MAP_REPOSITORY, new RoomUserMapRepository(new RoomUserMapDatasource()))
  }

  getRepository(repositoryName) {
    switch (repositoryName) {
      case USER_REPOSITORY: {
        return this.map.get(repositoryName)
      }
      case MESSAGE_REPOSITORY: {
        return this.map.get(repositoryName)
      }
      case ROOM_REPOSITORY: {
        return this.map.get(repositoryName)
      }
      case ROOM_USER_MAP_REPOSITORY: {
        return this.map.get(repositoryName)
      }
      default:
        throw new Error(`Unknown repository name - ${repositoryName}`)
    }
  }
}

module.exports.USER_REPOSITORY = USER_REPOSITORY
module.exports.ROOM_REPOSITORY = ROOM_REPOSITORY
module.exports.MESSAGE_REPOSITORY = MESSAGE_REPOSITORY
module.exports.ROOM_USER_MAP_REPOSITORY = ROOM_USER_MAP_REPOSITORY
