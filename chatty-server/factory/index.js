const MqttHandlerFactory = require('./MqttHandlerFactory')
const RepositoryFactory = require('./RepositoryFactory')
const ServiceFactory = require('./ServiceFactory')
const { USER_SERVICE, MESSAGE_SERVICE, ROOM_SERVICE } = require('./ServiceFactory')

const AuthController = require('../controller/AuthController')
const MessageController = require('../controller/MessageController')
const RoomController = require('../controller/RoomController')
const UserController = require('../controller/UserController')

module.exports = class AppFactory {
  constructor(app) {
    this.app = app
    this.repositoryFactory = new RepositoryFactory()
    this.serviceFactory = new ServiceFactory(this.repositoryFactory)
    this.mqttHandlerFactory = new MqttHandlerFactory(this.serviceFactory)

    this._setUpControllers()
    this.mqttHandlerFactory.setupHandlers()
  }

  _setUpControllers() {
    new AuthController(
      this.app,
      this.serviceFactory.getService(USER_SERVICE)
    )
    new MessageController(
      this.app,
      this.serviceFactory.getService(MESSAGE_SERVICE)
    )
    new RoomController(
      this.app,
      this.serviceFactory.getService(ROOM_SERVICE),
      this.serviceFactory.getService(MESSAGE_SERVICE),
      this.serviceFactory.getService(USER_SERVICE)
    )
    new UserController(
      this.app,
      this.serviceFactory.getService(ROOM_SERVICE),
      this.serviceFactory.getService(USER_SERVICE)
    )
  }

  _setUpMqttHandlers () {

  }
}