const RoomService = require('../service/RoomService')
const UserService = require('../service/UserService')

module.exports = class UserController {
  /**
   * @param {RoomService} roomService
   * @param {UserService} userService 
   */
  constructor(app, roomService, userService) {
    this.app = app
    this.roomService = roomService
    this.userService = userService
    this._setupRoutes()
  }

  async getAllUsers(req, res){
    try {
      const users = await this.userService.getAllUsers()
      res.status(200).json(users)
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: err.message
      })
    }
  }

  async getUserRooms(req, res) {
    const { id } = req.params

    try {
      const rooms = await this.roomService.getUserRooms(id)
      res.status(200).json(rooms)
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: err.message
      })
    }
  }

  async getUser(req, res) {
    const { id } = req.params

    try {
      const user = await this.userService.getUser(id)
      res.status(200).json(user)
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: err.message
      })
    }
  }

  _setupRoutes() {
    this.app.get('/users/all', (req, res) => this.getAllUsers(req, res))
    this.app.get('/users/:id', (req, res) => this.getUser(req, res))
    this.app.get('/users/:id/rooms', (req, res) => this.getUserRooms(req, res))
  }
}