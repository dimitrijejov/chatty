const User = require('../model/User')
const UserService = require('../service/UserService')

module.exports = class AuthController {
/**
   * @param {UserService} userService 
   */
  constructor(app, userService) {
    this.app = app
    this.userService = userService
    
    this._setupRoutes()
  }


  async register (req, res) {
    const { 
      phoneNumber,
      username,
      firstName,
      lastName
    } = req.body

    try {
      const result = await this.userService.createUser(
        new User({ phoneNumber, username, firstName, lastName })
      )
      res.status(201).json(result)
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: err.message 
      })
    }
  }

  _setupRoutes () {
    this.app.post('/register', (req, res) => this.register(req, res))
  }
}