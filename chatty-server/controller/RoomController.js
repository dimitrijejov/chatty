const Room = require('../model/Room')
const RoomService = require('../service/RoomService')
const MessageService = require('../service/MessageService')
const UserService = require('../service/UserService')

module.exports = class RoomController {
  /**
   * @param {RoomService} roomService
   * @param {MessageService} messageService
   * @param {UserService} userService
   */
  constructor(app, roomService, messageService, userService) {
    this.app = app
    this.roomService = roomService
    this.messageService = messageService
    this.userService = userService

    this._setupRoutes()
  }

  async createRoom(req, res) {
    const {
      name,
      roomUsers,
    } = req.body
    try {
      const room = await this.roomService.createRoom(
        new Room({ name }), roomUsers
      )
      res.status(201).json(room)
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: err.message
      })
    }
  }

  async getAllRooms(req, res) {
    try {
      const result = await this.roomService.getAllRooms()
      res.status(200).json(result)
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: err.message
      })
    }
  }

  async getRoom(req, res) {
    const {
      id
    } = req.params

    try {
      const result = await this.roomService.getRoom(id)
      res.status(200).json(result)
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: err.message
      })
    }
  }

  async getRoomLastMessage (req, res) {
    const { id } = req.params

    try {
      const result = await this.messageService.getRoomLastMessage(id)
      res.status(200).json(result)
    } catch (err) {
      console.log(err)
      res.status(400).json({
        status: 400,
        message: err.message
      })
    }
  }

  async getAllRoomMessages (req, res) {
    const { id } = req.params
    try {
      const result = await this.messageService.getAllRoomMessages(id)
      res.status(200).json(result)
    } catch (err) {
      console.log(err)
      res.status(400).json({
        status: 400,
        message: err.message
      })
    }
  }

  async getRoomUsers (req, res) {
    const { id } = req.params
    try {
      const result = await this.userService.getRoomUsers(id)
      res.status(200).json(result)
    } catch (err) {
      console.log(err)
      res.status(400).json({
        status: 400,
        message: err.message
      })
    }
  }

  _setupRoutes() {
    this.app.post('/rooms', (req, res) => this.createRoom(req, res))
    this.app.get('/rooms', (req, res) => this.getAllRooms(req, res))
    this.app.get('/rooms/:id', (req, res) => this.getRoom(req, res))
    this.app.get('/rooms/:id/last-message', (req, res) => this.getRoomLastMessage(req, res))
    this.app.get('/rooms/:id/messages', (req, res) => this.getAllRoomMessages(req, res))
    this.app.get('/rooms/:id/users', (req, res) => this.getRoomUsers(req, res))
  }
}
