const Message = require('../model/Message')
const MessageService = require('../service/MessageService')

module.exports = class MessageController {
  /**
   * @param {MessageService} messageService 
   */
  constructor (app, messageService) {
    this.app = app
    this.messageService = messageService
    this._setupRoutes()
  }

  async saveMessage(req, res) {
    const {
      roomId,
      senderId,
      content,
      isMedia
    } = req.body

    try {
      const result = await this.messageService.createMessage(
        new Message({ roomId, senderId, content, isMedia })
      )
      res.status(201).json(result)
    } catch (err) {
      res.status(400).json({ status: 400, message: err.message })
    }
  }


  _setupRoutes() {
    this.app.post('/messages', (req, res) => this.saveMessage(req, res))
  }
}