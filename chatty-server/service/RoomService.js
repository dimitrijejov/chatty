const RoomRepository = require('../data/Room/repository/RoomRepository')
const RoomUserMapRepository = require('../data/RoomUserMap/repository/RoomUserMapRepository')
const MessageRepository = require('../data/Message/repository/MessageRepository')
const UserRepository = require('../data/User/repository/UserRepository')
const RoomUserMap = require('../model/RoomUserMap')

const Room = require('../model/Room')

module.exports = class RoomService {
  /**
   * @param {RoomRepository} roomRepository 
   * @param {RoomUserMapRepository} roomUserMapRepository
   * @param {MessageRepository} messageRepository
   * @param {UserRepository} userRepository
   */
  constructor (roomRepository, roomUserMapRepository, messageRepository) {
    this.roomRepository = roomRepository
    this.roomUserMapRepository = roomUserMapRepository
    this.messageRepository = messageRepository
  }

  /**
   * @param {Room} room 
   * @param {Array[]} roomUsers
   */
 async createRoom (room, roomUsers) {
    const res = await this.roomRepository.createRoom(room)
    roomUsers.forEach( async (el) =>{
      await this.roomUserMapRepository.createRoomUserMap(
        new RoomUserMap({ roomId: res.id, userId: el})
      )
    })
    return res
  }

  getAllRooms () {
    return this.roomRepository.getAllRooms()
  }

  getRoom (id) {
    return this.roomRepository.getRoom(id)
  }

  async getUserRooms (userId) {
    const userRoomsMap = await this.roomUserMapRepository.getByUserId(userId)
    
    const rooms = await Promise.all(userRoomsMap.map(userRoomMap => (
      this.roomRepository.getRoom(userRoomMap.roomId)
    )))

    return rooms
  }
}
