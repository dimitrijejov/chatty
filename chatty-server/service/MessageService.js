const MessageRepository = require('../data/Message/repository/MessageRepository')
const { MqttClient } = require('../mqtt//MqttClient')
const Message = require('../model/Message')

module.exports = class MessageService {
  /**
   * @param {MessageRepository} messageRepository 
   * @param {MqttClient} mqttClient
   */
  constructor (messageRepository, mqttClient) {
    this.messageRepository = messageRepository
    this.mqttClient = mqttClient
  }

  /**
   * @param {number} roomId 
   * @returns {Promise<Message[]>}
   */
  getAllRoomMessages (roomId) {
    return this.messageRepository.getAllRoomMessages(roomId)
  }

  /**
   * @param {Message} message 
  */
  createMessage (message) {
    return this.messageRepository.createMessage(message)
  }

  /**
   * @param {number} roomId 
   */
  async getRoomLastMessage (roomId) {
    return this.messageRepository.getLastRoomMessage(roomId)
  }

  /**
   * 
   * @param {string} topic 
   * @param {Message} message 
   */
  async publishMessage (topic, message) {
    const stringMessage = JSON.stringify({ data: message.toJSON() })
    this.mqttClient.publish(topic, stringMessage)
  }
  
}