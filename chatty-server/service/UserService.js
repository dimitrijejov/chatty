const { UserMapper } = require('../data/User/repository/UserMapper')
const UserRepository = require('../data/User/repository/UserRepository')
const RoomUserMapRepository = require('../data/RoomUserMap/repository/RoomUserMapRepository')

const User = require('../model/User')

module.exports = class UserService {
  /**
   * @param {UserRepository} userRepository
   * @param {RoomUserMapRepository} roomUserMapRepository 
   */
  constructor(userRepository, roomUserMapRepository) {
    this.userRepository = userRepository
    this.roomUserMapRepository = roomUserMapRepository
  }
  /**
   * @param {User} user 
   */
  createUser(user) {
    return this.userRepository.createUser(user)
  }

  /**
   * 
   * @param {number} userId 
   */
  async getUser(userId) {
    return this.userRepository.getUser(userId)
  }

  async getAllUsers() {
    return this.userRepository.getAll()
  }


  /**
  * 
  * @param {number} roomId 
  */
  async getRoomUsers(roomId) {
    const roomUsersMap = await this.roomUserMapRepository.getByRoomId(roomId)

    const users = await Promise.all(roomUsersMap.map(roomUserMap => (
      this.userRepository.getUser(roomUserMap.userId)
    )))

    return users
  }
}
