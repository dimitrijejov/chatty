'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('room_user_map', {
      room_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
      }
    })

    await queryInterface.sequelize.query('ALTER TABLE "room_user_map" ADD COLUMN id SERIAL PRIMARY KEY')

    await queryInterface.sequelize.query('ALTER TABLE "room_user_map" ADD CONSTRAINT "fk_room" FOREIGN KEY ("room_id") REFERENCES "room" ("id") ON DELETE CASCADE ON UPDATE CASCADE')
    await queryInterface.sequelize.query('ALTER TABLE "room_user_map" ADD CONSTRAINT "fk_user" FOREIGN KEY ("user_id") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE CASCADE')
  },
  down: (queryInterface, Sequelize) => {}
}