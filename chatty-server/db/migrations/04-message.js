'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('message', {
      room_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      sender_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      content: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      is_media: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
      }
    })

    await queryInterface.sequelize.query('ALTER TABLE "message" ADD COLUMN id SERIAL PRIMARY KEY')

    await queryInterface.sequelize.query('ALTER TABLE "message" ADD CONSTRAINT "fk_room" FOREIGN KEY ("room_id") REFERENCES "room" ("id") ON DELETE CASCADE ON UPDATE CASCADE')
    await queryInterface.sequelize.query('ALTER TABLE "message" ADD CONSTRAINT "fk_user" FOREIGN KEY ("sender_id") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE CASCADE')
  },
  down: (queryInterface, Sequelize) => {}
}