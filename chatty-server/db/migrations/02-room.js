'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('room', {
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      direct_messaging: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
      }
    })

    await queryInterface.sequelize.query('ALTER TABLE "room" ADD COLUMN id SERIAL PRIMARY KEY')
  },
  down: (queryInterface, Sequelize) => {}
}