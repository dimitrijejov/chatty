'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('user', {
      phone_number: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      username: {
        type: Sequelize.STRING,
        allowNull: false
      },
      first_name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      last_name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true,
      }
    })

    await queryInterface.sequelize.query('ALTER TABLE "user" ADD COLUMN id SERIAL PRIMARY KEY')
    await queryInterface.sequelize.query('ALTER TABLE "user" ADD CONSTRAINT "u_user" UNIQUE ("phone_number", "username")')
  },
  down: (queryInterface, Sequelize) => {}
}