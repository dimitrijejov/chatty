const config = require('./config/config')
const AppFactory = require('./factory')

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

module.exports = class Server {
  constructor () {
    this.app = express()
    this.app.use(cors());
    this.app.use(bodyParser.json());
    this.appFactory = new AppFactory(this.app)
  }

  start() {
    this.app.listen(
      config.server.port,
      () => (
      console.log(`Server running on localhost:${config.server.port}`))
    )
  }
}