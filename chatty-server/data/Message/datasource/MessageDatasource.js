const config = require('../../../config/config')
const knex = require('knex')

module.exports = class MessageDatasource {
  constructor() {
    this.knex = new knex({
      client: 'pg',
      connection: {
        host: config.db.host,
        port: config.db.port,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database
      }
    })

    this.tableName = 'message'
  }

  getAllRoomMessages (roomId) {
    return this.knex.select().from(this.tableName)
      .where('room_id', roomId)
  }

  async getLastRoomMessage (roomId) {
    const result = await this.knex.select().from(this.tableName)
      .where('room_id', roomId)
      .orderBy('created_at', 'desc')
      .first()

    return result
  }

  async createMessage (message) {
    try {
      const result = await this.knex(this.tableName).insert(message).returning('*')
      return result[0]
    } catch (err) {
      throw new Error(err.detail)
    }
  }

  async updateMessage (message) {
    try {
      const results = await this.knex(this.tableName).update(message)
      .where('id', message.id)
      .returning('*')

      return results[0]
    } catch (err) {
      throw new Error(err.detail)
    }
  }

  deleteMessage (id) {
    return this.knex(this.tableName)
      .where('id', id)
      .del()
  }
}