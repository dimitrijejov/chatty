const { MessageMapper } = require('./MessageMapper')
const MessageDatasource = require('../datasource/MessageDatasource')
const Message = require('../../../model/Message')

module.exports = class MessageRepository {
  /**
   * @param {MessageDatasource} datasource
   */
  constructor(datasource) {
    this.messageDatasource = datasource
  }

  /**
   * @returns {Promise<Message[]>}
   */
  async getAllRoomMessages(roomId) {
    const messages = await this.messageDatasource.getAllRoomMessages(roomId)
    return messages.map(
      messageRow => MessageMapper.mapMessageRowToMessage(messageRow)
    )
  }

  /**
 * @returns {Promise<Message>}
 */
  async getLastRoomMessage(roomId) {
    const message = await this.messageDatasource.getLastRoomMessage(roomId)
    return MessageMapper.mapMessageRowToMessage(message)
  }

  /** 
   * @returns {Promise<Message>} message 
   */
  async createMessage(message) {
    const { id, ...restOfMessage } = message
    const createdMessage = await this.messageDatasource.createMessage(
      MessageMapper.mapMessageToMessageRow(restOfMessage)
    )

    return MessageMapper.mapMessageRowToMessage(createdMessage)
  }
}