const Message = require('../../../model/Message')

module.exports.MessageMapper = class MessageMapper {
  static mapMessageToMessageRow(message) {
    const messageRow = {
      room_id: message.roomId,
      sender_id: message.senderId,
      content: message.content,
      is_media: message.isMedia,
      created_at: message.createdAt,
      updated_at: message.updatedAt
    }

    if (message.id !== -1) {
      messageRow.id = message.id
    }

    return messageRow
  }

  static mapMessageRowToMessage(messageRow) {
    if (messageRow) {
      return new Message({
        id: messageRow.id,
        roomId: messageRow.room_id,
        senderId: messageRow.sender_id,
        content: messageRow.content,
        isMedia: messageRow.isMedia,
        updatedAt: messageRow.updated_at,
        createdAt: messageRow.created_at
      })
    } else {
      return null
    }
  }
}
