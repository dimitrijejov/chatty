const { UserMapper } = require("./UserMapper")
const UserDatasource = require('../datasource/UserDatasource')
const User = require('../../../model/User')

module.exports = class UserRepository {
  /**
   * @param {UserDatasource} datasource 
   */
  constructor(datasource) {
    this.userDatasource = datasource
  }

  /**
   * @returns {Promise<User[]>}
   */
  async getAll() {
    const userRows = await this.userDatasource.getAllUsers()
    return userRows.map(userRow => UserMapper.mapUserRowToUser(userRow))
  }

  /**
   * @param {string} phoneNum 
   * @returns {Promise<User>}
   */
  async getUserByPhoneNumber(phoneNum) {
    return this.userDatasource.getUserByPhoneNumber(phoneNum)
  }

  /**
   * @param {number} id
   * @returns {Promise<User>}
   */
  async getUser(id) {
    return UserMapper.mapUserRowToUser(
      await this.userDatasource.getUser(id)
    )
  }

  /**
 * @returns {Promise<User[]>}
 */
  async getAllRoomUsers(roomId) {
    const users = await this.userDatasource.getAllRoomUsers(roomId)
    return users.map(
      userRow => UserMapper.mapUserRowToUser(userRow)
    )
  }

  /**
   * @param {User} user 
   * @returns {Promise<User>}
   */
  async createUser(user) {
    const { id, ...restOfUser } = user
    const createdUser = await this.userDatasource.createUser(
      UserMapper.mapUserToUserRow(restOfUser)
    )

    return UserMapper.mapUserRowToUser(createdUser)
  }
}