const User = require("../../../model/User")

module.exports.UserMapper = class UserMapper {
  static mapUserToUserRow (user) {
    const userRow = {
      phone_number: user.phoneNumber,
      username: user.username,
      first_name: user.firstName,
      last_name: user.lastName,
      created_at: user.createdAt,
      updated_at: user.updatedAt
    }

    if (user.id || user.id !== -1) {
      userRow.id = user.id
    }

    if (user.updatedAt) {
      userRow.updated_at = user.updatedAt
    }

    return userRow
  }

  static mapUserRowToUser (userRow) {
    return new User({
      id: userRow.id,
      phoneNumber: userRow.phone_number,
      username: userRow.username,
      firstName: userRow.first_name,
      lastName: userRow.last_name,
      createdAt: userRow.created_at,
      updatedAt: userRow.updated_at
    })
  }
}