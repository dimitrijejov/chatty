const config = require('../../../config/config')
const knex = require('knex')

module.exports = class UserDatasource {
  constructor () {
    this.knex = new knex({
      client: 'pg',
      connection: {
        host: config.db.host,
        port: config.db.port,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database
      }
    })

    this.tableName = 'user'
  }

  getAllUsers () {
    return this.knex.select().from(this.tableName)
  }

  getUserByPhoneNumber (phoneNum) {
    return this.knex.select().from(this.tableName)
      .where('phone_number', phoneNum)
  }

  async getUser (id) {
    const result = await this.knex.select().from(this.tableName)
      .where('id', id)

    return result[0]
  }

  getUserByUsername (username){
    return this.knex.select().from(this.tableName)
      .where('username', username)
  }

  async createUser (user) {
    try {
      const result = await this.knex(this.tableName).insert(user).returning('*')
      return result[0]
    } catch (err) {
      throw new Error(err.detail)
    }
    
  }

  updateUser (user) {
    return this.knex(this.tableName).update(user)
      .where('id', user.id)
      .returning('*')
  }

  deleteUser (id) {
    return this.knex(this.tableName)
      .where('id', id)
      .del()
  }
}
