const { RoomUserMapMapper } = require('./RoomUserMapMapper')
const RoomUserMapDatasource = require('../datasource/RoomUserMapDatasource')

module.exports = class RoomUserMapRepository {
  /**
   * @param {RoomUserMapDatasource} datasource 
   */
  constructor (datasource) {
    this.roomUserMapDatasource = datasource
  }

  async getByRoomId (roomId) {
    const roomUserMaps = await this.roomUserMapDatasource.getByRoomId(roomId)

    return roomUserMaps.map(roomUserMapRow => (
      RoomUserMapMapper.mapRoomUserMapRowToRoomUserMap(roomUserMapRow)
    ))
  }

  async getByUserId (userId) {
    const roomUserMaps = await this.roomUserMapDatasource.getByUserId(userId)

    return roomUserMaps.map(roomUserMapRow => (
      RoomUserMapMapper.mapRoomUserMapRowToRoomUserMap(roomUserMapRow)
    ))
  }

  async createRoomUserMap (roomUserMap) {
    const createdMap = await this.roomUserMapDatasource.createRoomUserMap(
      RoomUserMapMapper.mapRoomUserMapToRoomUserMapRow(roomUserMap)
    )

    return RoomUserMapMapper.mapRoomUserMapRowToRoomUserMap(
      createdMap
    )
  }

  async updatedRoomUserMap (roomUserMap) {
    await this.roomUserMapDatasource.updateRoomUserMap(
      RoomUserMapMapper.mapRoomUserMapToRoomUserMapRow(roomUserMap)
    )

    return roomUserMap
  }

  async deleteRoomUserMap (id) {
    return this.roomUserMapDatasource.deleteRoomUserMap(id)
  }
}