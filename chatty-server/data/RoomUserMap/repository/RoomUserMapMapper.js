const RoomUserMap = require("../../../model/RoomUserMap")

module.exports.RoomUserMapMapper = class RoomUserMapMapper {
  static mapRoomUserMapToRoomUserMapRow (roomUserMap) {
    const row = {
      room_id: roomUserMap.roomId,
      user_id: roomUserMap.userId,
      created_at: roomUserMap.createdAt,
      updated_at: roomUserMap.updatedAt
    }

    if (roomUserMap.id !== -1) {
      row.id = roomUserMap.id
    }

    return row
  }

  static mapRoomUserMapRowToRoomUserMap (row) {
    return new RoomUserMap({
      id: row.id,
      roomId: row.room_id,
      userId: row.user_id,
      createdAt: row.created_at,
      updatedAt: row.updated_at
    })
  }
}