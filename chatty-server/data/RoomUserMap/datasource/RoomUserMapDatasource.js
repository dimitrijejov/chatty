const config = require('../../../config/config')
const knex = require('knex')

module.exports = class RoomUserMapDatasource {
  constructor () {
    this.knex = new knex({
      client: 'pg',
      connection: {
        host: config.db.host,
        port: config.db.port,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database
      }
    })
  }

  getByRoomId (roomId) {
    return this.knex.select().from('room_user_map')
      .where('room_id', roomId)
  }

  getByUserId (userId) {
    return this.knex.select().from('room_user_map')
      .where('user_id', userId)
  }

  async createRoomUserMap (roomUserMap) {
    try {
      const result = await this.knex('room_user_map')
        .insert(roomUserMap)
        .returning('*')

      return result[0]
    } catch (err) {
      throw new Error(err.detail)
    }
  }

  async updateRoomUserMap (roomUserMap) {
    try {
      const result = await this.knex('room_user_map')
        .update(roomUserMap)
        .where('id', roomUserMap.id)
        .returning('*')

      return result[0]
    } catch (err) {
      throw new Error(err.detail)
    }
    
  }

  async deleteRoomUserMap (id) {
    try {
      return this.knex('room_user_map')
        .where('id', id)
        .del()
    } catch (err) {
      throw new Error(err.detail)
    }
  }
}