const Room = require("../../../model/Room")

module.exports.RoomMapper = class RoomMapper {
  static mapRoomToRoomRow(room) {
    const roomRow = {
      name: room.name,
      direct_messaging: room.directMessaging,
      created_at: room.createdAt,
      updated_at: room.updatedAt
    }

    if (room.id || room.id !== -1) {
      roomRow.id = room.id
    }

    if (room.updatedAt) {
      roomRow.updated_at = room.updatedAt
    }

    return roomRow
  }

  static mapRoomRowToRoom(roomRow) {
    return new Room({
      id: roomRow.id,
      name: roomRow.name,
      directMessaging: roomRow.direct_messaging,
      createdAt: roomRow.created_at,
      updatedAt: roomRow.updated_at
    })
  }
}