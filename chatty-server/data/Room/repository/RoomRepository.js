const { RoomMapper } = require("./RoomMapper")
const RoomDatasource = require('../datasource/RoomDatasource')
const Room = require('../../../model/Room')

module.exports = class RoomRepository {
  /**
   * @param {RoomDatasource} datasource 
   */
  constructor(datasource) {
    this.roomDatasource = datasource
  }

  /**
   * @returns {Promise<Room[]>}
   */
  async getAllRooms() {
    const roomRows = await this.roomDatasource.getAllRooms()
    return roomRows.map(roomRow => RoomMapper.mapRoomRowToRoom(roomRow))
  }

  /**
    * @param {number} id
    * @returns {Promise<Room>} 
    */
  async getRoom(id) {
    const roomRows = await this.roomDatasource.getRoom(id)
    return RoomMapper.mapRoomRowToRoom(roomRows)
  }

  /**
   * @param {string} name
   * @returns {Promise<Room>} 
   */
  async getRoomByName(name) {
    return this.roomDatasource.getRoomByName(name)
  }

  /**
   * @param {Room} room 
   * @returns {notsure}
   */
  createRoom(room) {
    const { id, ...restOfRoom } = room
    return this.roomDatasource.createRoom(
      RoomMapper.mapRoomToRoomRow(restOfRoom)
    )
  }

}