const config = require('../../../config/config')
const knex = require('knex')

module.exports = class RoomDatasource {
  constructor() {
    this.knex = new knex({
      client: 'pg',
      connection: {
        host: config.db.host,
        port: config.db.port,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database
      }
    })
  }

  getAllRooms() {
    return this.knex.select().from('room')
  }

  async getRoom(id) {
    const result = await this.knex.select().from('room')
      .where('id', id)
    return result[0]
  }

  getRoomByName(name) {
    return this.knex.select().from('room')
      .where('name', name)
  }

  async createRoom(room) {
    try {
      const result = await this.knex('room').insert(room).returning('*')
      return result[0]
    } catch (err) {
      throw new Error(err.detail)
    }
  }

  updateRoom(room) {
    return this.knex('room').update(room)
      .where('id', room.id)
      .returning('*')
  }

  deleteRoom(id) {
    return this.knex('room')
      .where('id', id)
      .del()
  }
}
