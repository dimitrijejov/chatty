require('dotenv').config()

// SERVER
const serverPort = process.env.SERVER_PORT || 3000

// POSTGRES
const pgUser = process.env.PG_USER || ''
const pgHost = process.env.PG_HOST || ''
const pgDatabase = process.env.PG_DATABASE || ''
const pgPassword = process.env.PG_PASSWORD || ''
const pgPort = process.env.PG_PORT || ''

// MQTT
const mqttBrokerUrl = process.env.MQTT_BROKER_URL || ''


module.exports = {
  db: {
    user: pgUser,
    host: pgHost,
    database: pgDatabase,
    password: pgPassword,
    port: pgPort
  },
  server: {
    port: serverPort
  },
  mqtt: {
    brokerUrl: mqttBrokerUrl
  }
}